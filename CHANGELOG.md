# Change Log
All notable changes to the "atag-content-prompter" extension will be documented in this file.

## [0.6.0] - 2017-09-25
### Added
- Informational compliance prompt that appears when opening and saving files intended to bring Visual Studio Code into compliance with the 2017 refresh of 508 guidelines.