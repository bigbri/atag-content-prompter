# 508/WCAG Compliance Prompter README

This extension adds 508/WCAG 2.0 compliance prompts to Visual Studio Code when opening and saving files. These prompts are required for compliance with [part 504.3](https://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-ict-refresh/final-rule/text-of-the-standards-and-guidelines#504-authoring-tools) of [the 2017 refresh of the section 508 guidelines](https://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-ict-refresh/final-rule/text-of-the-standards-and-guidelines). Without the functionality described in 504.3, Visual Studio Code may be unusable by U.S. government agencies or other entities that require conformance to the access board's guidelines.

The extention's prompt informs the user to create content that conforms to Level A and AA Success Criteria and Conformance Requirements in WCAG 2.0. While it is likely that the extension must only display this message for HTML and Markdown files, the extension instead shows the prompt for all files in an effort to remain as compliant as possible.

This extension has no other effects on Visual Studio Code. The author makes no claims as to Visual Studio Code's compliance with 508 guidelines with or without this extension enabled.

## For more information

* [Text of the Standards and Guidelines - United States Access Board](https://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-ict-refresh/final-rule/text-of-the-standards-and-guidelines)
* [Section 508 Revised Report for Visual Studio Code](https://celaaatprod.blob.core.windows.net/public/7569f0a1-a759-4d4a-950f-1a627733e01c/34258516-e387-40d3-85a9-c73d76b60ad3/Visual%20Studio%20Code.Win32.RevisedSection508_082017.docx)